# Changelog

## 1.0.dev (2016-11-11)

- Initial version

# 1.0.0 (2017-11-15)

- Added some Environment variables
- Added service class from mbobject  

# 1.1.0 (2017-11-22)

- Added API REST Services for the queries requests   

# 1.1.1 (2017-11-28)

- Fix threading issue
