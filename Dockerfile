FROM python:2.7.12-alpine

ENV MBOBJECT_DIR /opt/mbobject/

COPY requirements.txt ./

RUN apk add --no-cache --virtual .build-deps build-base \
        && pip install -r requirements.txt \
        && find /usr/local \
                \( -type d -a -name test -o -name tests \) \
                -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
                -exec rm -rf '{}' + \
        && runDeps="$( \
                scanelf --needed --nobanner --recursive /usr/local \
                        | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                        | sort -u \
                        | xargs -r apk info --installed \
                        | sort -u \
        )" \
        && apk add --virtual .rundeps $runDeps \
        && apk del .build-deps

RUN mkdir -p $MBOBJECT_DIR
WORKDIR $MBOBJECT_DIR
COPY mb_timeseries_service/ $MBOBJECT_DIR/mb_timeseries_service
COPY test/ $MBOBJECT_DIR/test
WORKDIR $MBOBJECT_DIR/mb_timeseries_service
CMD [ "python", "timeseries_service.py" ]