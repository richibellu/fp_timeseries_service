MB Timeseries Service
=======================

Service to insert timeseries data points into InfluxDB 

## Requirements
- ubuntu 14.04
- Access to **mbobject_lib** repository
- Docker 1.12.3


## Make options
### Build docker container

  ```
  $ make build
  ```

### Run docker container

```
  $ make run

  # example running service in background
  $ make run-detached
```

## JSON format to add points in the queue

You can add a single point, or a list of points, in the queue to insert it in the InfluxDB. 

- **measurement:** String with the name of the measurement to insert the point.
- **tags:** Object with all the tag key-value pairs for the point.. Tag keys and tag values are both string. Optional.
- **fields:** Object with all field key-value pairs for the point. Field keys are strings. Field values can be floats, integers, strings, or booleans. Points must have at least one field.
- **time:** The timestamp for the data point. If the timestamp is not included with the point, the service uses the server’s local timestamp in UTC. The format can be a integer (Unix nanosecond timestamp) or a string (accepts:  `'YYYY-MM-DDTHH:mm:ssZ'`, `'YYYY-MM-DD HH:mm:ss'`, `'YYYY-MM-DD'`) 

####Example:

```
[
    {
        'measurement': 'sensor_input',
        'time': '2016-11-14T14:10:41Z',
        'tags': {
            'tid': '100101'
        },
        'fields': {
            'speed': 84.22,
            'elapsed_time':  26,
            'count': 42,
            'levelofservice': 'green'
        }
    }
]
```

**Note:** if the service receive an invalid point, this point is logged as an error and removed from the queue.
  
### Miscellaneous

How to kill process by name using grep
```
$ kill $(ps aux | grep 'python timeseries_service.py' | awk '{print $2}')
```

How do I remove all .pyc files from a project?
```
$ find . -name "*.pyc" -exec rm -rf {} \;
```