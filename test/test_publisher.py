import json
import logging
import time

import pika

logging.basicConfig()

# setup the parameter for the connection
credentials = pika.PlainCredentials('guest', 'guest')
params = pika.ConnectionParameters('localhost', 5672, '/', credentials)
params.socket_timeout = 5
# Connect to CloudAMQP
connection = pika.BlockingConnection(params)
# start a channel
channel = connection.channel()
# Declare the queue
channel.queue_declare(queue='timeseries_mq', durable=True)
# Declare the exchange
channel.exchange_declare('inserts_ts_msgs', type="topic", durable=True)
# Biding the exchange and the queue
channel.queue_bind(exchange='inserts_ts_msgs', queue='timeseries_mq', routing_key='timeseries_mq.*')
# properties for the publish
properties = pika.BasicProperties(
    delivery_mode=2,  # make message persistent
)
# create a message
dateInt = int(time.time() * 1000000000)
row = {'tid': '10', 'speed': 65, 'elapsed_time': 50, 'count': 30}
json_body = [
    {
        "measurement": "sensor_input",
        "tags": {
            "tid": row['tid']
        },
        "time": dateInt,
        "fields": {
            "speed": row['speed'],
            "elapsed_time":  row['elapsed_time'],
            "count": row['count'],
        }
    }
]
# infulxdb insert comand
# msg = 'sensor_input,tid={0} speed={1},elapsed_time={2},count={3} {4}'.format(row['tid'], row['speed'], row['elapsed_time'], row['count'], dateInt)
msg = json.dumps(json_body)
# send a message
channel.basic_publish(exchange='inserts_ts_msgs', routing_key='timeseries_mq.traffic', body=msg, properties=properties)
print " [x] Message sent to consumer: %r" % msg
connection.close()
