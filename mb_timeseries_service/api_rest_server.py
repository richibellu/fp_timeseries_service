import logging
import threading
from datetime import datetime
from itertools import cycle

import simplejson
from bottle import Bottle, template, PasteServer, response, request, tob

from mb_timeseries_service.query_handler import QueryHandler, datetimeToTimestamp

log = logging.getLogger(__name__)

# setting up the Bottle app
app = Bottle()


def default_error_handler(res):
    return tob(template("./resources/error_template", e=res))


app.default_error_handler = default_error_handler


@app.hook('after_request')
def enable_cors():
    '''Add headers to enable CORS'''
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-API-KEY'


@app.route('/', method='OPTIONS')
@app.route('/<path:path>', method='OPTIONS')
def options_handler(path=None):
    return


@app.get('/')
def heartbeat():
    return "It works!"


@app.get('/api-doc')
def api_doc():
    colors = cycle('#FFFFFF #CCCFDF'.split())
    docs_exclude = "/api-doc", "/api-map"
    routes = filter(lambda x: x.rule not in docs_exclude, app.routes)
    return template("./resources/doc_template", colors=colors, routes=routes)


def validate_param(rq, rs, param, post_params=False, required=True, allowEmpty=False, isFloat=False, isInt=False,
                   isDigit=False, isDatetime=False, minDigits=0, maxDigits=0, minValue=None, maxValue=None):
    try:
        # validate parameter
        if post_params:
            value = rq.forms.get(param)
        else:
            value = rq.params.get(param)
        if required:
            if value is None:
                raise ValueError("parameter %s is Null." % param)
            else:
                value = value.strip()
        if required and not allowEmpty:
            if not (value and value.strip()):
                raise ValueError("%s is Empty." % param)
            if len(value) < minDigits:
                raise ValueError("%s is lest that %s digits." % (param, minDigits))
            if 0 < maxDigits < len(value):
                raise ValueError("%s is more that %s digits." % (param, maxDigits))
        if value is not None and isFloat:
            try:
                value = float(value)
            except Exception:
                raise ValueError("'%s' must be a valid number." % param)
        elif value is not None and isInt:
            try:
                value = int(value)
            except Exception:
                raise ValueError("'%s' must be a valid integer." % param)
        elif value is not None and isDigit and not value.isdigit():
            raise ValueError("%s is not just digits." % param)
        if value is not None and minValue is not None and value < minValue:
            raise ValueError("%s have to be bigger thant %s ." % (param, minValue))
        if value is not None and maxValue is not None and value > maxValue:
            raise ValueError("%s have to be smaller thant %s ." % (param, maxValue))
        if value is not None and isDatetime:
            try:
                # noinspection PyTypeChecker
                value = datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')
            except Exception as e:
                log.warning("invalid %s format. %s" % (param, e))
                rs.status = 400
                return {'status': 400, 'message': "invalid %s format." % param}, False
    except Exception as e:
        msg = "invalid '%s' parameter. %s" % (param, e)
        log.warning(msg)
        rs.status = 400
        return {'status': 400, 'message': msg}, False
    return value, True


class RestServer(threading.Thread):
    def __init__(self, config):
        threading.Thread.__init__(self)
        self.config = config
        server_config = config['rest_server']
        self.host = server_config['host']
        self.port = server_config['port']
        self.prefix_mount = server_config['prefix_mount']
        self.route()
        self.app = None

    def run(self):
        while True:
            try:
                self.start_server()
            except Exception as e:
                log.error(e)

    def route(self):
        app.route('/traffic/report/sector/<sector_id>', method="GET", callback=self.getMobilityReportBySector)

    def start_server(self):
        if self.app:
            self.app.close()
        self.app = Bottle()
        self.app.mount(self.prefix_mount, app)
        self.app.run(host=self.host, port=self.port, debug=False, server=PasteServer)

    def getMobilityReportBySector(self, sector_id):
        """
        GET REQUEST

        This method extract from the time series all the traffic data related to a sector_id
        required by the Mobility FrontEnd


        URL params: Parameters included in the url request
        ==========
            -sector_id:     string. Id of the sector

        Params: GET PARAMS inserted passed in the request
        ======
            -from:              long. timestamp of the date when the period of the report starts.
                                    Default: 7 days before now()
            -to:                long. timestamp of the date when the period of the report ends.
                                    Default: now()
            -time_interval:     int. size in minutes for each block of time in the historical series.
                                    Default: 60 minutes

        responses:
        =========
        -JSON Object for each response
            {
                "sector_id" : {
                    "historical_data":{
                        [timestamp_1]: {
                            "speed": [speed_value]
                        },
                        ...,
                        [timestamp_n]: {
                            "speed": [speed_value]
                        }
                    },
                    "prediction_rmse": [rmse_value],
                    "prediction_accuracy": [accuracy_value],
                    "prediction_data":{
                        [timestamp_1]: {
                            "speed": [prediction_speed_value]
                            "anomaly_score": [anomaly_value]
                        },
                        ...,
                        [timestamp_m]: {
                            "speed": [prediction_speed_value]
                            "anomaly_score": [anomaly_value]
                        }
                    },
                    "matching_index":{
                        [timestamp_1]: {
                            "speed": [mi_speed_value]
                        },
                        ...,
                        [timestamp_n]: {
                            "speed": [mi_speed_value]
                        }
                    },
                }
            }
            """
        response.content_type = 'application/json'
        query_config = self.config['query_handler']
        default_range_size = query_config['default_range_size'] * 60
        default_time_interval = query_config['default_time_interval']
        time_block_size = query_config['time_block_size']
        max_range_size = query_config['max_range_size'] * 60
        now_timestamp = datetimeToTimestamp(datetime.utcnow())
        # validate GET parameters
        time_interval, valid = validate_param(request, response, 'time_interval', isInt=True,
                                              required=False, minValue=time_block_size)
        if valid is False:
            return time_interval
        elif not time_interval:
            time_interval = default_time_interval
        to_ts, valid = validate_param(request, response, 'to', isInt=True, required=False, minValue=max_range_size)
        if valid is False:
            return to_ts
        elif not to_ts or to_ts > now_timestamp:
            to_ts = now_timestamp
        from_ts, valid = validate_param(request, response, 'from', isInt=True, required=False,
                                        minValue=to_ts - max_range_size)
        if valid is False:
            return from_ts
        elif not from_ts:
            from_ts = to_ts - default_range_size

        queryHandler = QueryHandler(self.config)
        result = queryHandler.getMobilityReportBySector(sector_id=sector_id, from_ts=from_ts, to_ts=to_ts,
                                                        time_interval=time_interval)
        return simplejson.dumps(result, ignore_nan=True)
