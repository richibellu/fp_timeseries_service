import calendar
import logging
from collections import OrderedDict
from datetime import datetime

import numpy as np
from influxdb import DataFrameClient
from influxdb.resultset import ResultSet

log = logging.getLogger(__name__)
epoch_mpl = 1000000000


def getKeyFromTuple(item):
    if isinstance(item, tuple):
        if len(item) == 1:
            if isinstance(item[0], tuple):
                return getKeyFromTuple(item[0])
            else:
                return item[0]
        elif len(item) == 2:
            if isinstance(item[1], tuple):
                if isinstance(item[0], tuple):
                    return [getKeyFromTuple(item[0]), getKeyFromTuple(item[1])]
                else:
                    return getKeyFromTuple(item[1])
            else:
                return item[1]
        else:
            return [getKeyFromTuple(v) for v in item]
    else:
        return item


def epochToTimestamp(epoch):
    return epoch / epoch_mpl


def timestampToEpoch(timestamp):
    return timestamp * epoch_mpl


def datetimeToTimestamp(date_value):
    # return int(date_value.strftime("%s"))
    return calendar.timegm(date_value.utctimetuple())


def timeIndexToInt(df):
    return df.index.astype(int) / epoch_mpl


class QueryHandler:
    def __init__(self, config):
        self.query_config = config['query_handler']
        timeseries_config = config['timeseries']

        # create influx connection
        self.ts_client = DataFrameClient(host=timeseries_config["url"], port=timeseries_config["port"],
                                         username=timeseries_config["user"], password=timeseries_config["password"],
                                         database=timeseries_config["database"])

    def getMobilityReportBySector(self, sector_id, from_ts=None, to_ts=None, time_interval=None):
        log.debug("calling getMobilityReportBySector")
        historical_msmt = self.query_config['historical_query']['measurement']
        anomaly_msmt = self.query_config['anomaly_query']['measurement']
        indexes_msmt = self.query_config['indexes_query']['measurement']
        sector_id_tag = self.query_config['sector_id_tag']
        historical_fields = self.query_config['historical_query']['fields']
        anomaly_fields = self.query_config['anomaly_query']['fields']
        indexes_fields = self.query_config['indexes_query']['fields']
        indexes_name_field = self.query_config['indexes_query']['index_tag']
        indexes_name_value = self.query_config['indexes_query']['index_value']
        default_range_size = self.query_config['default_range_size'] * 60
        default_forecast_bound = self.query_config['default_forecast_bound'] * 60
        default_time_interval = self.query_config['default_time_interval']
        time_block_size = self.query_config['time_block_size']
        round_decimals = self.query_config['round_decimals']
        now_timestamp = datetimeToTimestamp(datetime.utcnow())
        # validate parameters
        if not time_interval:
            time_interval = default_time_interval
        if not to_ts or to_ts > now_timestamp:
            to_ts = now_timestamp
        if not from_ts:
            from_ts = to_ts - default_range_size

        function_field = 'MEAN'
        indexes_condition = "{}='{}'".format(indexes_name_field, indexes_name_value)

        result_hist = self.getData(historical_msmt, historical_fields, sector_id_tag, sector_id,
                                   from_ts, to_ts, function_field=function_field,
                                   function_interval=time_interval)
        result_hist_anomaly = self.getData(anomaly_msmt, anomaly_fields, sector_id_tag, sector_id,
                                           from_ts, to_ts,
                                           function_field=function_field, function_interval=time_interval)
        result_indexes = self.getData(indexes_msmt, indexes_fields, sector_id_tag, sector_id,
                                      from_ts, to_ts, indexes_condition, function_field=function_field,
                                      function_interval=time_interval)
        result = {}
        # Parse format for JSON
        for sid in result_hist:
            accuracy = 0
            rmse = -1
            df_anomaly = {}
            df_index = {}
            df_hist = result_hist[sid]
            df_hist.index = timeIndexToInt(df_hist)
            if sid in result_hist_anomaly:
                # calculate accuracy
                from_tail_ts = to_ts - default_forecast_bound
                if time_interval == time_block_size:
                    tail_hist = df_hist[historical_fields].loc[from_tail_ts:to_ts]
                    tail_hist = tail_hist.dropna()
                else:
                    tail_hist = self.getData(historical_msmt, historical_fields, sector_id_tag, sid,
                                             from_tail_ts, to_ts, function_field=function_field,
                                             function_interval=time_block_size)
                    if sid in tail_hist:
                        tail_hist = tail_hist[sid]
                        tail_hist.index = timeIndexToInt(tail_hist)
                        # add the last value registered the historical df
                        df_hist = df_hist.append(tail_hist.tail(1))
                        df_hist = df_hist.groupby(df_hist.index).last()
                        tail_hist = tail_hist[historical_fields]
                    else:
                        tail_hist = df_hist[historical_fields]

                result_prediction = self.getData(anomaly_msmt, anomaly_fields, sector_id_tag, sid,
                                                 from_tail_ts, to_ts + default_forecast_bound,
                                                 function_field=function_field, function_interval=time_block_size)
                result_prediction = result_prediction[sid]
                result_prediction.index = timeIndexToInt(result_prediction)
                if len(tail_hist) > 0:
                    tail_anomaly = result_prediction[anomaly_fields].reindex(tail_hist.index).dropna()
                    tail_hist = tail_hist.reindex(tail_anomaly.index).values
                    tail_anomaly = tail_anomaly.values
                    if len(tail_anomaly) > 0:
                        rmse = np.sqrt(((tail_hist - tail_anomaly) ** 2).mean())
                        tail_hist_mean = tail_hist.mean()
                        if tail_hist_mean == 0:
                            tail_hist_mean = 0.00001
                        accuracy = 1 - (rmse / tail_hist_mean)
                        if accuracy < 0:
                            accuracy = 0
                        accuracy = round(accuracy, round_decimals)
                        rmse = round(rmse, round_decimals)

                df_anomaly = result_hist_anomaly[sid]
                df_anomaly.index = timeIndexToInt(df_anomaly)
                df_anomaly = df_anomaly.append(result_prediction.loc[to_ts:])
                df_anomaly = df_anomaly.groupby(df_anomaly.index).last()
                df_anomaly = df_anomaly.rename(columns={anomaly_fields: 'Occupancy'}).round(round_decimals)
                df_anomaly = df_anomaly.to_dict('index')
            if sid in result_indexes:
                df_index = result_indexes[sid]
                df_index.index = timeIndexToInt(df_index)
                df_index = df_index.rename(columns={indexes_fields: 'speed'}).round(round_decimals).to_dict('index')
            sid_dict = []
            df_hist = df_hist.rename(columns={historical_fields: 'Occupancy'}).round(round_decimals)
            sid_dict.append(('historical_data', df_hist.to_dict('index')))
            sid_dict.append(('prediction_rmse', rmse))
            sid_dict.append(('prediction_accuracy', accuracy))
            sid_dict.append(('prediction_data', df_anomaly))
            sid_dict.append(('matching_index', df_index))
            result[sid] = OrderedDict(sid_dict)

        return result

    def getData(self, measurement, fields, group_by, tag_values, lower_bound=None, upper_bound=None,
                extra_condition=None, function_field=None, function_interval=5):
        log.debug("Getting data for {}: {}".format(group_by, tag_values))
        if not isinstance(fields, list):
            fields = [fields]

        if function_field:
            fields_str = ", ".join(["{0}({1}) as {1}".format(function_field, str(v)) for v in fields])
        else:
            fields_str = ", ".join([str(v) for v in fields])

        query = "SELECT {} FROM {} WHERE "
        query = query.format(fields_str, measurement)

        if lower_bound:
            lower_str = timestampToEpoch(lower_bound)
            query = query + " time >= {} AND".format(lower_str)
        if upper_bound:
            upper_str = timestampToEpoch(upper_bound)
            query = query + " time <= {} AND".format(upper_str)
        if extra_condition:
            query = query + " {} AND".format(extra_condition)
        if not isinstance(tag_values, list):
            tag_values = [tag_values]
        tags_str = "|".join(["^" + str(v) + "$" for v in tag_values])
        query = query + " {0}=~ /{1}/ GROUP BY {0} ".format(group_by, tags_str)

        if function_field:
            query = query + ", time({}m) fill(none)".format(function_interval)

        log.debug('Execute Query in timeseries:  %s ' % query)
        return self.executeTSQuery(query)

    def executeTSQuery(self, query):
        result_q = self.ts_client.query(query)
        if isinstance(result_q, ResultSet):
            return list(result_q.get_points())
        else:
            result_dict = {}
            for key in result_q:
                serie_tags_key = getKeyFromTuple(key)
                if isinstance(serie_tags_key, list):
                    serie_tags_key = ",".join([str(v) for v in serie_tags_key])
                result_dict[str(serie_tags_key)] = result_q[key]
            return result_dict
