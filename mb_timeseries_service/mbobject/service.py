# -*- coding: utf-8 -*-
import logging
import yaml
import os
import re
import sys

log = logging.getLogger(__name__)


"""
Add support to enviroment variables to yaml
Example:
    database_host: <%= ENV['HOST'] %>
"""

pattern = re.compile(r'^\<%= ENV\[\'(.*)\'\] %\>(.*)$')
yaml.add_implicit_resolver("!env", pattern)


def varaiblenv_constructor(loader, node):
    value = loader.construct_scalar(node)
    envVar, remainingPath = pattern.match(value).groups()
    data = yaml.load(os.environ[envVar])
    return data

yaml.add_constructor('!env', varaiblenv_constructor)


def setLogLevel(level):
    try:
        logging.getLogger().setLevel(getattr(logging, level.upper()))
    except Exception as e:
        log.exception(e)


def changeLogLevel(level, config_level):
    if level is None or level.upper() == "DEFAULT":
        level = config_level.upper()
    setLogLevel(level)


def initLog(level):
    log_format = "%(asctime)s %(name)-12s %(funcName)20s %(levelname)-8s %(message)s"
    logging.basicConfig(stream=sys.stdout, level=level, format=log_format)


def readConfig(filename):
    log.info("Reading config file {}".format(filename))
    try:
        with open(filename, 'r') as f:
            config = yaml.load(f)
            return config
    except Exception as e:
        log.exception(e)
        raise e


def serviceInit(config, config_file):
    initLog(logging.INFO)

    if not config:
        config = readConfig(config_file)

    loglevel = config["log"]["level"]
    setLogLevel(loglevel)
    return config
