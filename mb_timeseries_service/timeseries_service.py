import json
import logging
import sys
import threading
import time
import pika
import requests
from influxdb import InfluxDBClient
from mbobject import service
from api_rest_server import RestServer

log = logging.getLogger(__name__)


class TimeSeriesHandler(threading.Thread):
    def __init__(self, config):
        threading.Thread.__init__(self)
        self.config = config
        # setting up the parameters for queue consumer
        self.parameters = pika.URLParameters(config['queue']['url'])
        self.parameters.socket_timeout = 5
        self.parameters.connection_attempts = 10000000000000
        self.parameters.heartbeat_interval = 30
        self.ack = True
        self.timeseries = config['timeseries']
        self.queue_name = config['queue']['name']
        self.exchange = config['queue']['exchange']
        self.binding_key = config['queue']['binding_key']
        self.connection = None
        self.channel = None
        self.ts_client = None

    def connect(self):
        # set up subscription on the queue
        self.connection = pika.BlockingConnection(self.parameters)
        log.info("Connected to queue %s", self.queue_name)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(self.exchange, type="topic", durable=True)
        self.channel.queue_declare(queue=self.queue_name, durable=True)
        self.channel.queue_bind(exchange=self.exchange, queue=self.queue_name, routing_key=self.binding_key)
        self.ts_client = InfluxDBClient(host=self.timeseries['url'], port=self.timeseries['port'],
                                        username=self.timeseries['user'], password=self.timeseries['password'],
                                        database=self.timeseries['database'])
        # create the DB. If already exists, InfluxDB does not do anything.
        self.ts_client.create_database(self.timeseries['database'])

    def listen(self):
        log.info('Prepared to consume')
        self.connect()
        self.channel.basic_consume(self.incomingMessage, queue=self.queue_name, no_ack=not self.ack)
        self.channel.start_consuming()

    def close(self):
        log.debug('Close')
        self.channel.close()
        self.connection.close()

    def subscribe(self):
        log.info('Consumer Timeseries Initiated.')
        running = True
        while running:
            try:
                # Subscribe and start to consume
                self.listen()
            except pika.exceptions.ConnectionClosed as e:
                log.exception(e)
            except pika.exceptions.IncompatibleProtocolError as e:  # https://github.com/pika/pika/issues/732
                log.exception(e)
                time.sleep(5)
            except KeyboardInterrupt:
                log.warning('Killing timeseries_service by KeyboardInterrupt')
                running = False
            except Exception as e:
                log.exception(e)
                running = False
        self.close()

    # function which is called on incoming messages
    def incomingMessage(self, ch, method, properties, body):
        log.debug('msg: {}'.format(body))
        try:
            points = json.loads(body)
            if not isinstance(points, list):
                points = [points]
            # Inserts directly in the InfluxDB, If any pre-process is need it, it should be here.
            self.ts_client.write_points(points)
            log.info("timeseries points inserted:  %d", len(points))
            if self.ack:
                ch.basic_ack(delivery_tag=method.delivery_tag)
            return
        except requests.exceptions.ConnectionError as e:
            # If there is a connection error with the DB, raise the error and don't remove the msg from the queue
            log.error('DB ConnectionError: {0}  input: {1}'.format(e, body))
            raise e
        except Exception as e:
            # log.exception(e)
            log.error('{0}  input: {1}'.format(e, body))
            # TODO: should remove it from the queue? (Dead-Lettered Messages?)
            if self.ack:
                ch.basic_nack(delivery_tag=method.delivery_tag, requeue=False)
            return

    def run(self):
        while True:
            try:
                self.subscribe()
            except Exception as e:
                log.error(e)

if __name__ == '__main__':
    print('Starting Timeseries Service.')
    conf = None
    if len(sys.argv) == 2:
        conf = json.loads(sys.argv[1])
    conf = service.serviceInit(conf, './resources/config.yml')
    timeSeriesHandler = TimeSeriesHandler(config=conf)
    log.info('Starting Queue Handler.')
    timeSeriesHandler.start()

    # setting up the Bottle REST server
    if 'rest_server' in conf:
        restServer = RestServer(config=conf)
        log.info('Starting Rest Server.')
        restServer.start()
