import json
import sys
from mb_timeseries_service.mbobject import service
from mb_timeseries_service.api_rest_server import RestServer


# main
if __name__ == '__main__':

    print('Starting Timeseries Service.')
    conf = None
    if len(sys.argv) == 2:
        conf = json.loads(sys.argv[1])

    conf = service.serviceInit(conf, './resources/config.yml')
    restServer = RestServer(config=conf)
    restServer.start()