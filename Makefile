DOCKER_NAME=worldsensing/mb_timeseries_service
VERSION=1.1.1
CONNECTOR_CONTAINER_NAME=mb_timeseries_service
DOCKER_NAME_FULL=$(DOCKER_NAME):$(VERSION)
DOCKER_LOCALHOST=$(shell /sbin/ifconfig docker0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $$1}')
DOCKER_VOLUME=$(shell pwd)
API_PORT=8181
CONFIG= -e "DS_TS_SERVICE_NAME=mb"\
		-e "DS_TS_SERVICE_LOG_LEVEL=INFO"\
		-e "DS_TS_SERVICE_INFLUX_USER=root"\
		-e "DS_TS_SERVICE_INFLUX_PASS=root"\
		-e "DS_TS_SERVICE_INFLUX_URL=mbinfluxdb"\
		-e "DS_TS_SERVICE_INFLUX_PORT=8086"\
		-e "DS_TS_SERVICE_DATABASE=mobility"\
		-e "DS_TS_SERVICE_QUEUE_URL=amqp://mbrabbitmq/"\
		-e "DS_TS_SERVICE_QUEUE_BINDING=timeseries_mq.*"\
		-e "DS_TS_SERVICE_QUEUE_NAME=timeseries_mq"\
		-e "DS_TS_SERVICE_QUEUE_EXCHANGE=inserts_ts_msgs"\
		-e "DS_TS_SERVICE_API_PORT=$(API_PORT)"\
		-e "DS_TS_SERVICE_API_URL_PREFIX=ts/api/v1"\
		-e "DS_TS_QUERY_SECTOR_ID_TAG=sid"\
		-e "DS_TS_QUERY_HISTORICAL_MS=traffic_regular_data"\
		-e "DS_TS_QUERY_HISTORICAL_FIELD=currentSpeed"\
		-e "DS_TS_QUERY_ANOMALY_MS=traffic_anomaly"\
		-e "DS_TS_QUERY_ANOMALY_PREDICTION_FIELD=predictionSpeed"\
		-e "DS_TS_QUERY_ANOMALY_ANOMALY_FIELD=anomaly"\
		-e "DS_TS_QUERY_INDEXES_MS=traffic_index"\
		-e "DS_TS_QUERY_INDEXES_FIELD=speed"\
		-e "DS_TS_QUERY_INDEXES_TAG=index"\
		-e "DS_TS_QUERY_INDEXES_VALUE=matching_index"\
		-e "DS_TS_QUERY_DEFAULT_RANGE_SIZE=10080"\
		-e "DS_TS_QUERY_MAX_RANGE_SIZE=43200"\
		-e "DS_TS_QUERY_FORECAST_BOUND=60"\
		-e "DS_TS_QUERY_TIME_INTERVAL=60"\
		-e "DS_TS_QUERY_TIME_BLOCK_SIZE=5"\
		-e "DS_TS_QUERY_ROUND_DECIMALS=4"

clean:
	@find . -iname "*~" | xargs rm 2>/dev/null || true
	@find . -iname "*.pyc" | xargs rm 2>/dev/null || true
	@find . -iname "build" | xargs rm -rf 2>/dev/null || true

minify: clean
	@mkdir -p build/minified && pyminifier --obfuscate --destdir=build/minified/ **/*.py

build: clean
	docker build -t $(DOCKER_NAME_FULL) .

build-minified: minify
	@cp Dockerfile requirements.txt build/
	docker build -t $(DOCKER_NAME_FULL) build/

run:
	@docker run -it --add-host mbrabbitmq:$(DOCKER_LOCALHOST) --add-host mbinfluxdb:$(DOCKER_LOCALHOST) \
		-p $(API_PORT):$(API_PORT) \
        --name	$(CONNECTOR_CONTAINER_NAME) \
        $(CONFIG) --rm $(DOCKER_NAME_FULL) $(CMD)

run-detached:
	@docker run -i --add-host mbrabbitmq:$(DOCKER_LOCALHOST) --add-host mbinfluxdb:$(DOCKER_LOCALHOST) \
		-p $(API_PORT):$(API_PORT) \
        --name	$(CONNECTOR_CONTAINER_NAME) \
        $(CONFIG) -d $(DOCKER_NAME_FULL) $(CMD)

publish: build
	@docker push $(DOCKER_NAME_FULL)

publish-minified: build-minified
	@docker push $(DOCKER_NAME_FULL)

tag_confirmation:
	@read -p "Are you sure to continue to create a tag (y/n)?" choice;\
		case "$$choice" in\
		y|Y ) echo "yes";;\
		n|N ) exit 1;;\
		* ) exit 1;;\
		esac

tag: tag_confirmation publish
	vim CHANGELOG.md
	@git tag $(CONNECTOR_CONTAINER_NAME)_$(VERSION)
	@git push origin --tags